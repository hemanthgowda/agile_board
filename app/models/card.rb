class Card < ApplicationRecord
	STATUS = {BACKLOG: 'Backlog', INPROGRESS: 'InProgress',DONE: 'Done',QAVERIFIED: 'QAVerified'}
	TICKET_TYPES = {NORMAL: 'Normal',MAJOR: 'Major',CRITICAL: 'Critical',SHOWSTOPPER: 'Show-stopper'}
	belongs_to :user
	belongs_to :assignee, class_name: 'User', foreign_key: :assignee_id

	after_update :send_email

	def send_email
		#emailer logic
		#CardsMailer.notify_update(id).deliver_later
	end
end

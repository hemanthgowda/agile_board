json.extract! card, :id, :title, :description, :status, :user_id, :assignee_id, :due_date, :ticket_type, :created_at, :updated_at
json.url card_url(card, format: :json)

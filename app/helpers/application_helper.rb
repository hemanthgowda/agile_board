module ApplicationHelper
	def users
	  options = {}
	  users = User.all
	  users.each {|user| options = options.merge({user.email => user.id})}
	  options
	end

	def status
		options = {}
		Card::STATUS.each {|st| options = options.merge({st[1] => st[1]})}
		options
	end

	def ticket_types
		options = {}
		Card::TICKET_TYPES.each {|st| options = options.merge({st[1] => st[1]})}
		options
	end
end

class CreateCards < ActiveRecord::Migration[5.1]
  def change
    create_table :cards do |t|
      t.text :title
      t.text :description
      t.string :status
      t.integer :user_id
      t.integer :assignee_id
      t.date :due_date
      t.string :ticket_type

      t.timestamps
    end
  end
end

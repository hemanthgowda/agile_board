# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: 'demo@gmail.com', password: 'password', password_confirmation: 'password')

status = ['Backlog', 'InProgress', 'Done', 'QAVerified']
ticket_types = ['Normal', 'Critical', 'Major']
25.times do
	Card.create(title: (0...8).map { (65 + rand(26)).chr }.join, description:(0...8).map { (65 + rand(26)).chr }.join, status: status.sample, ticket_type: ticket_types.sample, user_id: 1, assignee_id: 1)
end
